package CRUD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SingleConnection {

	private static Connection con;
	private SingleConnection () {
		String url = "jdbc:mysql://localhost/hospitalNFA019_jdbc";
		String login = "root";
		String password = "";
		try {
			con = DriverManager.getConnection(url, login, password);
		}catch (SQLException e){
			e.printStackTrace();
		}		
	}
	private SingleConnection (String url, String login, String password) {
		try {
			con = DriverManager.getConnection(url, login, password);
		}catch (SQLException e){
			e.printStackTrace();
		}		
	}
	
	public static Connection getInstance() {
		if(con==null) {
			new SingleConnection();	
		}
		return con;
	}
	public static Connection getInstance(String url, String login, String password) {
		if(con==null) {
			new SingleConnection(url, login, password);
		}
		return con;
	}
	public static void connectionClose ()  {
		try{
			con.close ();
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	
}
