package CRUD;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Hopital.Patient;

public class crudSQL {

	private String url;
	private String login;
	private String password;
	private Connection con;

	public crudSQL(String url, String login, String password) throws ClassNotFoundException {
		this.url = url;
		this.login = login;
		this.password = password;
		this.con = SingleConnection.getInstance(url, login, password);
		Class.forName("com.mysql.cj.jdbc.Driver");
	}

	public crudSQL() {
		String url = "jdbc:mysql://localhost/hospitalnfa019_jdbc";
		String urllogin = "root";
		String urlpassword = "";
		try {
		this.con = SingleConnection.getInstance(url, urllogin, urlpassword);
		Class.forName("com.mysql.cj.jdbc.Driver");
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void crudClose() {
		SingleConnection.connectionClose();
	}


	public Patient findByID(int id) throws SQLException {
		String requet = "SELECT * FROM patients WHERE idpatient=" + Integer.toString(id) + ";";
		Patient pat = new Patient();
		Statement stmt = this.con.createStatement();
		System.out.println(requet);
		ResultSet rs = stmt.executeQuery(requet);
		try {
			while (rs.next()) {
				pat.setIdpatient(rs.getInt(1));
				pat.setNom(rs.getString(2));
				pat.setPrenom(rs.getString(3));
				pat.setDateNaissance(rs.getDate(4));
				pat.setPathologies(rs.getString(5));
				pat.setAntecedents(rs.getString(6));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException se) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException se) {
				}
//			if (con != null)
//				try {
//					con.close();
//				} catch (SQLException se) {}
		}
		return pat;
	}

	public boolean newPatient(Patient pat) throws SQLException {
		boolean res = false;
		String requet = "INSERT INTO patients "	+ 
				"(id,nom,prenom,datenaissance,pathologies,antecedents)" + 
				" VALUES (";
		requet += pat.getIdpatient() + ",";
		requet += "'" + pat.getNom() + "', ";
		requet += "'" + pat.getPrenom() + "', ";
		requet += "'" + pat.getDateNaissance() + "', ";
		requet += "'" + pat.getPathologies() + "', ";
		requet += "'" + pat.getAntecedents() + "');";

		Statement stmt = this.con.createStatement();
		System.out.println(requet);
		try {
			int i = stmt.executeUpdate(requet);
			if (i > 0)
				res = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException se) {
				}

		}
		return res;
	}

	public int updatePatient (Patient pat) throws SQLException {
		int res=0;
		Statement stmt = this.con.createStatement();
		try {
			String requet = "UPDATE patients SET ";
			requet += "'" + pat.getNom() + "', ";
			requet += "'" + pat.getPrenom() + "', ";
			requet += "'" + pat.getDateNaissance() + "', ";
			requet += "'" + pat.getPathologies() + "', ";
			requet += "'" + pat.getAntecedents() + "'";
			requet += " WHERE id=" + Integer.toString(pat.getIdpatient()) + ";";
			System.out.println(requet);
			res = stmt.executeUpdate(requet);
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException se) {
			}
		}
		return res;
	}

	public int delete(Patient pat) throws SQLException {
		int res = 0;
		Statement stmt = this.con.createStatement();
		try {
			String requet = "DELETE FROM patients ";
			requet += " WHERE id=" + Integer.toString(pat.getIdpatient()) + ";";
			System.out.println(requet);
			res = stmt.executeUpdate(requet);
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (stmt != null) 	try {
					stmt.close();
				} catch (SQLException se) {
				}
		}
		return res;
	}




}
