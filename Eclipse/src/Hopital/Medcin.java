package Hopital;

import java.util.*;


public class Medcin {

	private int idMedecin;
	private String nom;
	private String prenom;
	private String specialisation;
	private Date [] creneauTravaille;
	private int bureau;
	public Medcin(int idMedecin, String nom, String prenom, String specialisation, Date[] creneauTravaille,
			int bureau) {
		super();
		this.idMedecin = idMedecin;
		this.nom = nom;
		this.prenom = prenom;
		this.specialisation = specialisation;
		this.creneauTravaille = creneauTravaille;
		this.bureau = bureau;
	}
	public int getIdMedecin() {
		return idMedecin;
	}
	public void setIdMedecin(int idMedecin) {
		this.idMedecin = idMedecin;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getSpecialisation() {
		return specialisation;
	}
	public void setSpecialisation(String specialisation) {
		this.specialisation = specialisation;
	}
	public Date[] getCreneauTravaille() {
		return creneauTravaille;
	}
	public void setCreneauTravaille(Date[] creneauTravaille) {
		this.creneauTravaille = creneauTravaille;
	}
	public int getBureau() {
		return bureau;
	}
	public void setBureau(int bureau) {
		this.bureau = bureau;
	}	
}