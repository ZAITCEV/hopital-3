package Hopital;

import java.util.*;

public class Patient {
		
	private int idpatient;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	private String pathologies;
	private String antecedents;
	
	public Patient(int idpatient, String nom, String prenom, Date dateNaissance, String pathologies,String antecedents) {
		super();
		this.idpatient = idpatient;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.pathologies = pathologies;
		this.antecedents = antecedents;
	}
	public Patient() {
		this.idpatient = 0;
		this.nom = "";
		this.prenom = "";
		this.dateNaissance = new Date();
		this.pathologies = "";
		this.antecedents = "";
	}
	public int getIdpatient() {
		return idpatient;
	}
	public void setIdpatient(int idpatient) {
		this.idpatient = idpatient;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Date getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getPathologies() {
		return pathologies;
	}
	public void setPathologies(String pathologies) {
		this.pathologies = pathologies;
	}
	public String getAntecedents() {
		return antecedents;
	}
	public void setAntecedents(String antecedents) {
		this.antecedents = antecedents;
	}
	@Override
	public String toString() {
		return "Patient [idpatient=" + idpatient + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance="
				+ dateNaissance + ", pathologies=" + pathologies + ", antecedents=" + antecedents + "]";
	}
	


}