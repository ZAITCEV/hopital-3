package Hopital;

import java.util.Date;

public class Consultations {

	public int idconsultation;
	public Date dateconsult;
	public int idpatient;
	public int idmedecin;
	public String detaills;
	public String propositions;
	public String traitement;
	public String appareil;
	
	public Consultations(int idconsultation, Date dateconsult, int idpatient, int idmedecin, String detaills,
			String propositions, String traitement, String appareil) {
		this.idconsultation = idconsultation;
		this.dateconsult = dateconsult;
		this.idpatient = idpatient;
		this.idmedecin = idmedecin;
		this.detaills = detaills;
		this.propositions = propositions;
		this.traitement = traitement;
		this.appareil = appareil;
	}

	public int getIdconsultation() {
		return idconsultation;
	}

	public void setIdconsultation(int idconsultation) {
		this.idconsultation = idconsultation;
	}

	public Date getDateconsult() {
		return dateconsult;
	}

	public void setDateconsult(Date dateconsult) {
		this.dateconsult = dateconsult;
	}

	public int getIdpatient() {
		return idpatient;
	}

	public void setIdpatient(int idpatient) {
		this.idpatient = idpatient;
	}

	public int getIdmedecin() {
		return idmedecin;
	}

	public void setIdmedecin(int idmedecin) {
		this.idmedecin = idmedecin;
	}

	public String getDetaills() {
		return detaills;
	}

	public void setDetaills(String detaills) {
		this.detaills = detaills;
	}

	public String getPropositions() {
		return propositions;
	}

	public void setPropositions(String propositions) {
		this.propositions = propositions;
	}

	public String getTraitement() {
		return traitement;
	}

	public void setTraitement(String traitement) {
		this.traitement = traitement;
	}

	public String getAppareil() {
		return appareil;
	}

	public void setAppareil(String appareil) {
		this.appareil = appareil;
	}
	
	
	
	
	
}
