CREATE TABLE patients (
    idpatient integer NOT NULL,
    nom character varying(64) NOT NULL,
    prenom character varying(64) NOT NULL,
    datenaissance DATE NOT NULL,
	pathologies character varying(512),
	antecedents TEXT,
	PRIMARY KEY (idpatient)
);

INSERT INTO 
patients (idpatient,nom,prenom,datenaissance,pathologies,antecedents)
VALUES (1,'DUBOIS','Dominique','1970-04-01',NULL,NULL);


INSERT INTO 
patients (idpatient,nom,prenom,datenaissance,pathologies,antecedents)
VALUES (2,'FHALI','Aboubacar','1995-04-15','allergie au poisson',NULL);


INSERT INTO 
patients (idpatient,nom,prenom,datenaissance,pathologies,antecedents)
VALUES (3,'JARAH','Mylene','1989-09-24',NULL,NULL);


INSERT INTO 
patients (idpatient,nom,prenom,datenaissance,pathologies,antecedents)
VALUES (4,'ANTIBUS','Antibus','2012-02-20','diabet',NULL);


INSERT INTO 
patients (idpatient,nom,prenom,datenaissance,pathologies,antecedents)
VALUES (5,'KVAZIBUS','Kvazibus','2012-02-20','diabet',NULL);
